This theme is a port for Drupal of the andreas01-theme as found on 
http://openwebdesign.org/viewdesign.phtml?id=2199

This theme requires PHPTemplate.
It validates as XHTML1.1 and CSS2

Thanks to SEO Position for contributing the theme. http://www.seoposition.com/