<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>">

<head>
    <title><?php print $head_title ?></title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <style type="text/css" media="screen">@import "<?php print base_path(). path_to_theme(); ?>/seoposition.css";</style>
    <style type="text/css" media="projection">@import "<?php print base_path(). path_to_theme(); ?>/seoposition.css";</style>
    <style type="text/css" media="print">@import "<?php print base_path(). path_to_theme(); ?>/print.css";</style>
    <?php print $styles ?>
</head>
<body>
<?php /* CONTENT WRAPPER */ ?>
<div id="globalwrap">
<?php /* TOP HEADER BEGIN */ ?>
<div id="header">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
</div>
<?php /* END HEADER */ ?>
<?php /* NAVIGATION BEGIN */ ?>
<div id="nav">
<div id="subNav">
  <?php if (is_array($secondary_links)) { ?>
  	<?php $i = 0;foreach ($secondary_links as $link) : if ($i==0) {?>
			<?php
			} else {?>
			<?php
			}?><?php print $link ?>
        <?php $i++; 
	             endforeach; ?>
      <?php } ?>
  </div>
</div>
<?php /* END HEADER */ ?>
<?php $wider_screen = array('block','settings','themes'); // added for more admin usability, add more as needed ?>
<?php /* MAIN CONTENT BEGINS */ ?>
<div id="content" <?php if (in_array(arg(1),$wider_screen)) { ?> style="width : 850px;"<?php } ?>>
<?php /* LEFT COLUMN BEGIN */ ?>
<div id="primary">
<?php print $breadcrumb ?>
<?php if ($mission) { ?><div id="mission"><p><?php print $mission ?></p></div><?php } ?>
      <?php print $help ?>
      <?php print $messages ?>
	  
	  <?php if ($title) { ?><h1 class="title"><?php print $title ?></h1><?php } ?>
      <div class="tabs"><?php print $tabs ?>
</div>
<?php /* END LEFT COLUMN */ ?>
<?php print $content; ?>
<?php /* END MAIN CONTENT */ ?>
</div>
<?php /* RIGHT COLUMNS BEGINS */ ?>
<div id="secondary">
      <?php if ($sidebar != "") { ?>
        <?php print $sidebar ?>
      <?php }; ?>
<?php /* END RIGHT COLUMN */ ?>
<?php /* RIGHT COLUMN CATEGORIES BEGIN */ ?>
      <div id="secondary1">	
		<div id="mainservices">
	<?php if ($sidebar_left != "") { ?>
	<?php print $sidebar_left ?>
    </div>
    <?php }; ?>
	</div>
<?php /* FAR RIGHT COLUMN BEGINS */ ?>
<?php if ($sidebar_right != "") { ?>
    <div id="secondary2">
		<div id="consumption">
	<?php print $sidebar_right ?>
    </div>
  <?php }; ?>

<?php /* END FAR RIGHT COLUMN */ ?>
</div>
<?php /* END RIGHT COLUMN CATEGORIES */ ?>
</div>
<?php /* END MAIN CONTENT */ ?>
<?php /* FOOTER BEGINS */ ?>
</div>	
 <div class="footer" id="footer"><h6><?php print $footer_message ?> Designed by <a href="http://www.seoposition.com">SEO Position</a></h6></div>
  <?php print $closure ?>
<?php /* END FOOTER */ ?>
</div>
<?php /* END CONTENT WRAPPER */ ?>
</body>
</html>